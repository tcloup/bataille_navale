# Bataille Navale:

## D�pendances

- Symfony
- Xampp ou Wampp (Windows)
- [.NET 6](https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/runtime-desktop-6.0.5-windows-x64-installer)

## Lancement

- T�l�chargez le source code contenu dans ce repo,

- Extraire le contenu du fichier .zip dans un nouveau fichier, s�par� de tout autres �l�ments

- Dans une console, collez la commande suivante:

`symfony server:start`

- Installez .NET 6 Desktop Runtime (et non pas Runtime)(le lien ci-dessus renvois vers la bonne version)

- T�l�chargez le [websocket](https://gitlab.univ-lr.fr/tcloup/bataille-navale-websocket/-/releases/permalink/latest) pour votre plateforme si ce n'est pas d�j� fais 

- Extraire le contenu du fichier .zip t�l�charg� dans un autre nouveau dossier vide et s�par� de tout autres �l�ments

- Configurez votre Database de mani�re appropri� dans `settings.json`

- Lancez le websocket en double cliquant sur le fichier executable `WebSocket.exe` (Windows) ou `./Websocket` (Linux & Windows)

- Connecter vous sur le site web via [http://localhost:8000](http://localhost:8000)

- Pour commencer � jouer, cr�ez un compte via la page d'inscription puis connectez vous

- Une fois connect�, allez vers [http://localhost:8000/play](http://localhost:8000/play) et vous devriez pouvoir commencer � jouer






