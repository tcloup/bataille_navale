var src;
var attacking = false;
var state = 0;
var myturn = false;

document.addEventListener("DOMContentLoaded", _ => {
    src = new WebSocket("ws://" + ip + ":7272");

    src.onopen = _ => {
        src.send(JSON.stringify(
            {
                "Secret": secret,
                "RoomId": roomId
            }
        ));

        src.send(JSON.stringify(
            {
                "Action": "start"
            }
        ));
    }

    src.onmessage = message => {

        if (message.data == "ping")
        {
            src.send("pong");
            return;
        }

        if (message.data == "NextTurn")
        {
            document.getElementById("state-dependant-text").textContent = "Choose a tile to attack";
            myturn = true;
            return;
        }

        parsed = JSON.parse(message.data);

        if (parsed.State != undefined)
        {
            switch(parsed.State)
            {
                // Placement phase
                case 1:
                    document.getElementById("state-dependant-text").textContent = "Choose a boat";
                    createGrid();
                    state = 1;
                    break;
                // Attack phase
                case 2:
                    document.getElementById("state-dependant-text").textContent = "Waiting for Opponent's turn to end";
                    removeCommands();
                    removePlaceEventListeners();
                    createOpponentGrid();
                    addHitEventListeners();
                    state = 2;
                    break;
                // Game end
                case 3:
                    document.getElementById("state-dependant-text").textContent = parsed.Info;
                    state = 3;
                    break;
            }
            console.log(`State ${parsed.State}`)
        }

        if (parsed.Result != undefined && parsed.Result == "Success")
        {
            if (state == 1) {
                placeBoatSuccess(parsed);
            }
        }
        else if(parsed.Result != undefined && parsed.Result == "hit")
        {
            if (state == 2 )
            {
                if (myturn == true)
                {
                    attackTileSuccess(parsed);
                }
                else
                {
                    attackHitAlly(parsed);
                }
            }
        }
        else if (parsed.Result != undefined && parsed.Result == "miss")
        {
            if (state == 2 )
            {
                if (myturn == true)
                {
                    attackTileMissed(parsed);
                }
                else
                {
                    attackMissedAlly(parsed);
                }
            }
        }
    }

    let debugButton = document.getElementById("state-select-button");

    if (debugButton != undefined)
        debugButton.addEventListener("click", _ => {
            switch (document.getElementById("state-select").value)
            {
                case "0":
                    let grid = document.getElementsByClassName("grid")[0];

                    while (grid.firstChild)
                    {
                        document.getElementsByClassName("grid")[0].removeChild(grid.firstChild);
                    }

                    state = 0;

                    removeCommands();
                    debugSendState(0);
                    break;

                case "1":
                    let pgrid = document.getElementsByClassName("grid")[0];

                    while (pgrid.firstChild)
                    {
                        document.getElementsByClassName("grid")[0].removeChild(pgrid.firstChild);
                    }

                    state = 1;

                    debugSendState(1);
                    break;

                case "2":
                    state = 2;

                    removeCommands();
                    removePlaceEventListeners();
                    addHitEventListeners();

                    debugSendState(2);
                    break;

                case "3":
                    state = 3;
                    
                    AddEnd();
                    debugSendState(3);
                    break;
            }
        })
})



function createGrid()
{
    // Grid Setup Phase
    let grid = document.getElementsByClassName("grid")[0];

    for(let x=0; x < 10; x++) {
        let ol = document.createElement("ol");

        for(let y=0; y < 10; y++) {
            let li = document.createElement("li");
            let button = document.createElement("button");
            button.addEventListener("click", _ => placeBoat(x, y));

            li.appendChild(button);
            ol.appendChild(li);
        }

        grid.appendChild(ol);
    }

    document.getElementsByClassName("commands")[0].classList.remove("hidden");

    return;
}

function createOpponentGrid()
{
    let opponent_grid = document.getElementsByClassName("opponent-grid")[0];

    for(let x=0; x < 10; x++) {
        let ol = document.createElement("ol");

        for(let y=0; y < 10; y++) {
            let li = document.createElement("li");
            let button = document.createElement("button");

            li.appendChild(button);
            ol.appendChild(li);
        }

        opponent_grid.appendChild(ol);
    }
}

function removePlaceEventListeners()
{
    let grid = document.getElementsByClassName("grid")[0];

    for(let x=0; x < 10; x++) {
        for(let y=0; y < 10; y++) {
            let cell = grid.querySelectorAll("ol")[x].querySelectorAll("button")[y];

            cell.replaceWith(cell.cloneNode(true));
        }
    }
}

function removeCommands()
{
    document.getElementById("boat-select").classList.add("hidden");
}


function addHitEventListeners()
{
    let grid = document.getElementsByClassName("opponent-grid")[0].querySelectorAll("ol");

    for (let x = 0; x < grid.length - 1; x++)
    {
        for (let y = 0; y < grid.length - 1; y++)
        {
            let cell = grid[x].querySelectorAll("button")[y];

            cell.addEventListener("click", _ => attackTile(x, y));
        }
    }
}


function placeBoat(x, y)
{
    let id = document.getElementById("boat-select").value;

    let data = {
        "Action": "place",
        "Id": parseInt(id),
        "Position": {
            "X": x,
            "Y": y
        }
    };

    src.send(JSON.stringify(data));
}

function placeBoatSuccess(data)
{
    console.log(`Placing boat with ID ${data.Boat}`)

    let Size = JSON.parse(data.Size);
    let Position = JSON.parse(data.Position);

    // remove the boat as a choice
    document.getElementById("boat-select").querySelectorAll("option").forEach(item => {
        if (parseInt(item.getAttribute("value")) == data.Boat)
        {
            item.remove();

            if (document.getElementById("boat-select").querySelectorAll("option").length == 0)
            {
                document.getElementById("boat-select").classList.add("hidden");
                document.getElementById("state-dependant-text").textContent = "Waiting for Opponent's turn to end";
            }
        }
    })

    let grid = document.getElementsByClassName("grid")[0];

    for (var xi = 0; xi < Size[0]; xi++)
    {
        for (var yi = 0; yi < Size[1]; yi++)
        {
            let cell = grid.querySelectorAll("ol")[Position.X + xi].querySelectorAll("button")[Position.Y + yi];

            cell.classList.add("boat");

            cell.replaceWith(cell.cloneNode(true));
        }
    }
}

function attackTile(x, y)
{
    let data = {
        "Action": "attack",
        "Position": {
            "X": x,
            "Y": y
        }
    }

    src.send(JSON.stringify(data));
}

function attackTileSuccess(data)
{
    let grid = document.getElementsByClassName("opponent-grid")[0];

    let cell = grid.querySelectorAll("ol")[data.Position.X].querySelectorAll("button")[data.Position.Y];

    cell.classList.add("hit");

    cell.replaceWith(cell.cloneNode(true));

    document.getElementById("state-dependant-text").textContent = "Waiting for Opponent's turn to end";
    myturn = false;
}

function attackHitAlly(data)
{
    let grid = document.getElementsByClassName("grid")[0];

    let cell = grid.querySelectorAll("ol")[data.Position.X].querySelectorAll("button")[data.Position.Y];

    cell.classList.replace("boat", "hit");
}

function attackTileMissed(data)
{
    let grid = document.getElementsByClassName("opponent-grid")[0];

    let cell = grid.querySelectorAll("ol")[data.Position.X].querySelectorAll("button")[data.Position.Y];

    cell.classList.add("miss");

    cell.replaceWith(cell.cloneNode(true));

    document.getElementById("state-dependant-text").textContent = "Waiting for Opponent's turn to end";
    myturn = false;
}

function attackMissedAlly(data)
{
    let grid = document.getElementsByClassName("grid")[0];

    let cell = grid.querySelectorAll("ol")[data.Position.X].querySelectorAll("button")[data.Position.Y];

    cell.classList.add("miss")
}

function removeEnd()
{
    let end = document.getElementById("end-text");

    if (end != undefined)
    {
        end.remove();
    }
}


function AddEnd()
{
    let end = document.createElement("p");
    end.textContent = "Game ended";
    end.id = "end-text";
    document.body.appendChild(end);
}


function debugSendState(value)
{
    let data = {
        "Action": "debug",
        "SubAction": "state",
        "Value": value
    }

    src.send(JSON.stringify(data));
}