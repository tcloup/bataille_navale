<?php

namespace App\Controller;

use App\Entity\Boat;
use App\Entity\Grid;
use App\Entity\Player;
use App\Entity\User;
use App\Entity\Gamemode;
use App\Form\FormNewUserType;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Room;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $grid = $doctrine->getManager()->getRepository(Grid::class)->find(1);

        return $this->render('default/index.html.twig', [
            "id" => 1,
            "grid" => json_encode($grid->getCells())
        ]);
    }

    #[Route('/register', name: 'register')]
    public function NewUser(ManagerRegistry $doctrine, Request $request, UserPasswordHasherInterface $passwordHasher )
    {
        $user = new User();
        $form = $this->createForm(FormNewUserType::class, $user);
        $form->handleRequest($request);
        $data = $form->getData();
        if ($form->isSubmitted() && $form->isValid()) {
            $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $data->getPassword()
            );
            $user->setPassword($hashedPassword);
            $em = $doctrine->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('homepage');
        }
        return $this->render('default/formNewUser.html.twig', ['form' => $form->createView()]);
    }

    #[Route('/ranking', name: 'ranking')]
    public function ranking(UserRepository $userRepository): Response
    {
        $classement = $userRepository->classment();

        return $this->render('default/ranking.html.twig', [
            "id" => 0,
            'rang' => 0,
            'classement' => $classement
        ]);
    }



    #[Route('/play', name: 'play')]
    public function play(ManagerRegistry $doctrine): Response
    {
        $repo = $doctrine->getManager()->getRepository(Room::class);

        $qb = $repo->createQueryBuilder('r');

        $rooms = $qb->select('r')
            ->where('r.slots > 0')
            ->andWhere('r.state = 0')
            ->getQuery()
            ->execute();

        if (count($rooms) > 0)
        {
            return $this->redirect("/play/{$rooms[count($rooms) - 1]->getId()}");
        }
        else
        {
            $room = new Room();

            $doctrine->getManager()->persist($room);
            $doctrine->getManager()->flush();

            return $this->redirect("/play");
        }
    }

    #[Route('/play/{id}', name: 'playId')]
    public function playId(ManagerRegistry $doctrine, int $id): Response
    {
        $user = $this->getUser();

        if ($user == null)
        {
            return $this->redirectToRoute("homepage");
        }

        /*$repo = $doctrine->getManager()->getRepository(Room::class);

        $room = $repo->find($id);

        if ($room->getState() == 0) {
            if ($room->getSlots() > 0 && !$this->isUserInRoom($doctrine, $room, $user)) {
                $player = new Player();
                $player->setUser($user);
                $player->setRoom($room);

                $grid = $doctrine->getManager()->getRepository(Grid::class)->find(1);

                $player->setGrid($grid->getCells());

                $gamemode = $doctrine->getManager()->getRepository(Gamemode::class)->find(1);

                $player->setBoats($gamemode->getBoats());

                $room->addPlayer($player);

                $doctrine->getManager()->persist($player);
                $doctrine->getManager()->persist($room);
                $doctrine->getManager()->flush();
            }
        }*/

        return $this->render('default/play.html.twig', [
            "id" => $id,
            "ip" => gethostbyname(gethostname()),
            "secret" => $user->getSecret()
        ]);
    }

    /**
     * @route("/update/room/{id}", name="game", methods={"POST"})
     *
     * @IsGranted("ROLE_USER", statusCode=403, message="Why would you send a request to a room you're not part of, Anno")
     */
    public function game(HubInterface $hub, ManagerRegistry $doctrine, Request $request, int $id): Response
    {
        $user = $this->getUser();

        $repo = $doctrine->getManager()->getRepository(Room::class);

        $room = $repo->find($id);

        if (!$this->isUserInRoom($doctrine, $room, $user)) {
            return new Response(
                json_encode(
                    array(
                        "result" => "error",
                        "message" => "Why would you send a request to a room you're not part of, {$user->getUserIdentifier()} ?"
                    )
                )
            );
        }

        $player = $room->getPlayers()[0]->getUser()->getId() == $user->getId() ? $room->getPlayers()[0] : $room->getPlayers()[1];
        $opponent = $room->getPlayers()[0]->getUser()->getId() == $user->getId() ? $room->getPlayers()[1] : $room->getPlayers()[0];

        $data =  json_decode($request->request->get("data"));

        $action = $data->body->action ?? "";

        // If All slots are taken, go to the next game phase, the placement phase
        if ($action == "start")
        {
            if ($room->remaining_slots == 0)
            {
                $update = new Update("/update/room/{$id}", 
                json_encode(array(
                    "state" => 1
                )));

                $hub->publish($update);

                return new Response("Success");
            }
        }
        if ($action == "leaving")
        {
            if ($room->getState() != 0 || $room->getState() != 3)
            {
                $update = new Update("/update/room/{$id}",
                    json_encode(array(
                        "info" => snprintf("%s Won the game!", $opponent->getUser()->getSpeudo()),
                        "detail" => "Opponent disconnected"
                    )));

                $hub->publish($update);

                $room->state = 3;

                $doctrine->getManager()->persist($room);
            }
        }

        // Process request and game in another, separate function
        $updateData = $this->ProcessGame($doctrine, $room, $player, $data->body);

        // If everyone placed their boats, go to the next game phase, the Attack phase
        if ($room->getState() == 1)
        {
            $placed = 0;
            foreach($room->getPlayers() as $player)
            {
                foreach($player->getBoats() as $boat)
                {
                    if ($boat instanceof(Boat::class))
                    {
                        if ($boat->getPosition() != [])
                        {
                            $placed++;
                        }
                    }
                }
            }

            if ($placed == 10)
            {
                $update = new Update("/update/room/{$id}",
                    json_encode(array(
                        "state" => 2
                    )));

                $hub->publish($update);
            }
        }

        // TODO: Only parts that changed should be in the returned data
        if ($action == "attack")
        {
            $update = new Update("/update/room/{$id}",
                json_encode($updateData)
            );

            $hub->publish($update);
        }
        else
        {
            return new Response(json_encode($updateData));
        }

        return new Response("Success");
    }

    public function ProcessGame(ManagerRegistry $doctrine, Room $room, Player $player, stdClass $data): array
    {
        switch($data->action)
        {
            case "place":
                if ($room->getState() != 1)
                    return array(
                        "result" => "error",
                        "message" => "Current game isn't in placement period."
                    );

                if (isset($data->id))
                    $boat = $this->getBoat($player, $data->id);
                else
                    return array(
                        "result" => "error",
                        "message" => "no boat ID provided"
                    );

                if ($boat == null)
                    return array(
                        "result" => "error",
                        "message" => sprintf("No boat with id %d", $data->id)
                    );

                if (isset($action->position) && $action->position instanceof stdClass)
                {
                    if (isset($action->position->x) && isset($action->position->y))
                    {
                        if (is_int($action->position->x) && is_int($action->position->y))
                        {
                            if ($player->placeBoat($boat, $action->position->x, $action->position->y))
                            {
                                $boat->setPosition([$action->position->x, $action->position->y]);

                                $boats = $player->getBoats() ;
                                $boats[$boat->getId()] = $boat;
                                $player->setBoats($boats);

                                $doctrine->getManager()->persist($player);

                                return array(
                                    "result" => "success",
                                    "boat" => $boat->getId(),
                                    "position" => array(
                                        "x" => $action->position->x,
                                        "y" => $action->position->y
                                    ),
                                    "size" => $boat->getSize()
                                );
                            }
                            else
                            {
                                return array(
                                    "result" => "error",
                                    "message" => sprintf("%s (id = %d) cannot be placed at (%d;%y)",
                                        $boat->getName(),
                                        $boat->getId(),
                                        $action->position->x,
                                        $action->position->y
                                    )
                                );
                            }
                        }
                    }
                }

                return array(
                    "result" => "Unknown",
                    "message" => "Invalid position value"
                );

            case "attack":
                //room si les id sont les memes on récupère le premier joueur sinon c'est le second
                $player = $room->getPlayers()[0]->getUser()->getId() == $this->getUser()->getId() ? $room->getPlayers()[1] : $room->getPlayers()[0];
                $turn = $room->getTurn();
                $cell = $player -> getGrid() [$data->position->x][$data->position->y];

                if($cell->hit == false )
                {
                    if ($cell->boat == null)
                    {
                        $room->setTurn($turn == 0 ? 1 : 0);
                        return array(
                            "result" => "missed",
                            "position" => array(
                                "X" => $data->position->x,
                                "Y" => $data->position->y,
                            )
                        );
                    }
                    else
                    {
                        $cell->hit = true;
                        $cell->boat->health--;

                        /*if ($cell->boat->health == 0)
                        {
                            return array(
                                "result" => "drowned",
                                "position" => array(
                                    "X" => $data->position->x,
                                    "Y" => $data->position->y,
                                ));
                        }*/

                        return array(
                            "result" => "hit",
                            "position" => array(
                                "X" => $data->position->x,
                                "Y" => $data->position->y,
                            )
                        );
                    }
                }
                else
                {
                    return array(
                        "result" => "error",
                        "message"=>"%s (id = %d) cannot be placed at (%d;%y)",
                    );
                }

                break;

            default:
                return array(
                    "result" => "error",
                    "message" => "action is invalid or not provided."
                );
        }

        return array(
            "result" => "Unknown",
            "message" => "Is this a cosmic ray?"
        );
    }

    /**
     * Une fonction pour tester mercure, utilisé lors du setup de mercure
     *
     * @Route("/update/test/room/{id}", name="test")
     *
     * @IsGranted("ROLE_USER", statusCode=403, message="Why would you send a request to a room you're not part of, Anno")
     */
    public function test(ManagerRegistry $doctrine, HubInterface $hub, ?int $id): Response
    {
        $user = $this->getUser();

        $repo = $doctrine->getManager()->getRepository(Room::class);

        $room = $repo->find($id);

        if (!$this->isUserInRoom($doctrine, $room, $user))
        {
            return new Response("Failure");
        }

        $player = $room->getPlayers()[0]->getUser()->getId() == $user->getId() ? $room->getPlayers()[0] : $room->getPlayers()[1];

        $boat = new Boat();

        $boat->setSize([2, 1]);

        $update = new Update(sprintf("/update/test/%s", $id),
            json_encode(
                array(
                    "result" => "success",
                    "data" => sprintf("boat %s be placed", $player->placeBoat($boat, 10, 10) ? "can" : "cannot")
                )
            )
        );

        $hub->publish($update);

        return new Response("Success");
    }

    public function getBoat(Player $player, int $id): ?Boat
    {
        if ($id < count($player->getBoats()))
            return $this->cast($player->getBoats()[$id], Boat::class);

        return null;
    }

    public function isUserInRoom(ManagerRegistry $doctrine, Room $room, User $user): bool
    {
        foreach ($room->getPlayers() as $player)
        {
            if ($player->getUser()->getId() == $user->getId())
            {
                return true;
            }
        }

        return false;
    }

    protected function cast(StdClass $instance, string $className): mixed
    {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            \strlen($className),
            $className,
            strstr(strstr(serialize($instance), '"'), ':')
        ));
    }
}
