<?php

namespace App\Controller\Admin;

use App\Entity\Grid;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class GridCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Grid::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
