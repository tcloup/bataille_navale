<?php

namespace App\Entity;

use App\Repository\GamemodeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GamemodeRepository::class)]
class Gamemode
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'json')]
    private $boats = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBoats(): ?array
    {
        return $this->boats;
    }

    public function setBoats(array $boats): self
    {
        $this->boats = $boats;

        return $this;
    }
}
