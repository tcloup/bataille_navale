<?php

namespace App\Entity;

use App\Repository\RoomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RoomRepository::class)]
class Room
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToMany(mappedBy: 'room', targetEntity: Player::class)]
    private $players;

    /*
     * 0 = Game hasn't started
     * 1 = Placement Period
     * 2 = Attack period
     * 3 = Game finished
     */
    #[ORM\Column(type: 'integer')]
    private $state = 0;

    #[ORM\Column(type: 'integer')]
    private $slots = 2;

    #[ORM\Column(type: 'integer')]
    private $turn = 0;

    public function __construct()
    {
        $this->players = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Player>
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function addPlayer(Player $player): self
    {
        if (!$this->containsPlayer($player) && $this->slots > 0) {
            $this->players[] = $player;
            $this->setSlots($this->getSlots() - 1);
            $player->setRoom($this);
        }

        return $this;
    }

    public function removePlayer(Player $player): self
    {
        if ($this->players->removeElement($player)) {
            // set the owning side to null (unless already changed)
            if ($player->getRoom() === $this) {
                $player->setRoom(null);
            }
        }

        return $this;
    }

    public function containsPlayer(Player $player): bool
    {
        foreach($this->players as $p)
        {
            if ($player->getId() == $p->getId())
                return true;
        }

        return false;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getSlots(): ?int
    {
        return $this->slots;
    }

    public function setSlots(int $slots): self
    {
        $this->slots = $slots;

        return $this;
    }

    public function getTurn(): ?int
    {
        return $this->turn;
    }

    public function setTurn(int $turn): self
    {
        $this->turn = $turn;

        return $this;
    }
}
