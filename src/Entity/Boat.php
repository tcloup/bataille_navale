<?php

namespace App\Entity;

use App\Repository\BoatRepository;
use Doctrine\ORM\Mapping as ORM;

class Boat
{
    private $id;

    private $position = [];

    private $size = [];

    private $health;

    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPosition(): ?array
    {
        return $this->position;
    }

    public function setPosition(array $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getSize(): ?array
    {
        return $this->size;
    }

    public function setSize(array $size): self
    {
        $this->size = $size;
        $this->setHealth($size[0] + $size[1]);

        return $this;
    }

    public function getHealth(): ?array
    {
        return $this->health;
    }

    public function setHealth(int $health): self
    {
        $this->health = $health;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
