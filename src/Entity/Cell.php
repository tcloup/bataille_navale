<?php

namespace App\Entity;

class Cell
{
    private $id;

    private $isPlayable;

    private $isOccupied;

    private $hit;

    private $boat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsPlayable(): ?bool
    {
        return $this->isPlayable;
    }

    public function setIsPlayable(bool $isPlayable): self
    {
        $this->isPlayable = $isPlayable;

        return $this;
    }

    public function getIsOccupied(): ?bool
    {
        return $this->isOccupied;
    }

    public function setIsOccupied(bool $isOccupied): self
    {
        $this->isOccupied = $isOccupied;

        return $this;
    }

    public function getHit(): ?bool
    {
        return $this->hit;
    }

    public function setHit(bool $hit): self
    {
        $this->hit = $hit;

        return $this;
    }

    public function getBoat(): ?Boat
    {
        return $this->boat;
    }

    public function setBoat(Boat $boat): self
    {
        $this->boat = $boat;

        return $this;
    }
}
